package eu.uhcfr.caoutchouc;

import net.minecraft.launchwrapper.LaunchClassLoader;
import net.minecraft.launchwrapper.LogWrapper;
import org.apache.logging.log4j.Level;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

/**
 * Copyright (c) 2015 - 2017 UHCFr. All rights reserved
 * This file is a part of UHCFr project.
 *
 * @author Gogume1er
 */
class CaoutchoucCorePluginLoader {

    private final List<Class<?>> corePluginsClass = new ArrayList<>();
    private final List<CorePlugin> corePlugins = new ArrayList<>();

    void loadCorePlugin(File file) {
        try {
            JarFile jarFile = new JarFile(file);
            Manifest manifest = jarFile.getManifest();

            if(manifest == null)
                throw new FileNotFoundException("Jar does not contain manifest");

            String corePlugin = manifest.getMainAttributes().getValue("corePlugin");

            if(corePlugin == null)
                throw new IllegalArgumentException("Manifest does not contain \"corePlugin\" tag");


            Caoutchouc.addInClassLoader(file);
            Class<?> corePluginClass = Class.forName(corePlugin);

            if(CorePlugin.class.isAssignableFrom(corePluginClass))
                this.corePluginsClass.add(corePluginClass);

        } catch(Exception exc) {
            LogWrapper.log(Level.ERROR, exc, "Unable to load core plugin %s", file.getName());
        }
    }

    void initCorePlugins() {
        for(Class<?> corePluginClass : this.corePluginsClass) {
            try {
                Object object = corePluginClass.newInstance();

                if(!(object instanceof CorePlugin))
                    continue;

                this.corePlugins.add((CorePlugin) object);
            } catch(Exception exc) {
                LogWrapper.log(Level.ERROR, exc, "Unable to init core plugin %s", corePluginClass.getName());
            }
        }
    }

    void injectCorePluginsIntoClassLoader(LaunchClassLoader launchClassLoader) {
        this.corePlugins.sort((o1, o2) -> o1.getTweakOrder() == o2.getTweakOrder() ?
                0 : (o1.getTweakOrder() > o2.getTweakOrder() ? 1 : -1));

        this.corePlugins.forEach(corePlugin -> {
            for(String transformer : corePlugin.getTransformerClass())
                launchClassLoader.registerTransformer(transformer);
        });
    }


}
