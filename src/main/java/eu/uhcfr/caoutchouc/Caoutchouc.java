package eu.uhcfr.caoutchouc;

import net.minecraft.launchwrapper.Launch;
import net.minecraft.launchwrapper.LogWrapper;
import org.apache.logging.log4j.Level;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Copyright (c) 2015 - 2017 UHCFr. All rights reserved
 * This file is a part of UHCFr project.
 *
 * @author Gogume1er
 */
public class Caoutchouc {

    private static Caoutchouc instance;

    private CaoutchoucCorePluginLoader corePluginLoader;

    private void launch(String[] args) {
        Caoutchouc.instance = this;
        this.corePluginLoader = new CaoutchoucCorePluginLoader();

        LogWrapper.info("Loading Caoutchouc...");

        replaceArgs(args);

        File pluginsFile = new File("coreplugins/");

        if(!pluginsFile.exists())
            pluginsFile.mkdirs();

        for(File file : pluginsFile.listFiles())
            if(file.isFile() && file.getName().endsWith(".jar")) {
                LogWrapper.info("Loading core plugin %s", file.getName());
                this.corePluginLoader.loadCorePlugin(file);
            }

        Launch.main(args);
    }

    private void replaceArgs(String[] args) {
        for(int i = 0; i < args.length; i++) {
            String arg = args[i];

            if(!"--serverJar".equals(arg)) {
                if(i == args.length - 2)
                    throw new IllegalArgumentException("Cannot found server argument.");
                else
                    continue;
            }

            String serverURL = args[i + 1];
            File serverFile = new File(serverURL);

            if(!serverFile.exists())
                throw new IllegalArgumentException("Cannot found server jar.");

            Caoutchouc.addInClassLoader(serverFile);

            args[i] = "--tweakClass";
            args[i + 1] = "eu.uhcfr.caoutchouc.CaoutchoucTweaker";
            break;
        }
    }

    static void addInClassLoader(File file) {
        try {
            Method method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
            URLClassLoader systemClassLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
            method.setAccessible(true);
            method.invoke(systemClassLoader, file.toURI().toURL());
        } catch(Exception exc) {
            LogWrapper.log(Level.ERROR, exc, "Unable to add library into the ClassLoader");
        }
    }

    CaoutchoucCorePluginLoader getCorePluginLoader() {
        return this.corePluginLoader;
    }

    static Caoutchouc getInstance() {
        return Caoutchouc.instance;
    }

    public static void main(String[] args) {
        new Caoutchouc().launch(args);
    }

}
