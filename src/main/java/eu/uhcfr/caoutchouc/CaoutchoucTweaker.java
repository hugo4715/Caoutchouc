package eu.uhcfr.caoutchouc;

import net.minecraft.launchwrapper.ITweaker;
import net.minecraft.launchwrapper.LaunchClassLoader;

import java.io.File;
import java.util.List;

/**
 * Copyright (c) 2015 - 2017 UHCFr. All rights reserved
 * This file is a part of UHCFr project.
 *
 * @author Gogume1er
 */
public class CaoutchoucTweaker implements ITweaker {

    private List<String> args;

    @Override
    public void acceptOptions(List<String> list, File file, File file1, String s) {
        this.args = list;
    }

    @Override
    public void injectIntoClassLoader(LaunchClassLoader launchClassLoader) {
        launchClassLoader.addClassLoaderExclusion("eu.uhcfr.caoutchouc.");
        Caoutchouc.getInstance().getCorePluginLoader().initCorePlugins();
        Caoutchouc.getInstance().getCorePluginLoader().injectCorePluginsIntoClassLoader(launchClassLoader);
    }

    @Override
    public String getLaunchTarget() {
        return "org.bukkit.craftbukkit.Main";
    }

    @Override
    public String[] getLaunchArguments() {
        return this.args.toArray(new String[this.args.size()]);
    }

}
