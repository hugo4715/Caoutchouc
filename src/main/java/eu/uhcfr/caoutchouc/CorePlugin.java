package eu.uhcfr.caoutchouc;

/**
 * Copyright (c) 2015 - 2017 UHCFr. All rights reserved
 * This file is a part of UHCFr project.
 *
 * @author Gogume1er
 */
public interface CorePlugin {

    int getTweakOrder();

    String[] getTransformerClass();

}
