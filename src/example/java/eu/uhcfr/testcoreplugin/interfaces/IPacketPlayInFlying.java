package eu.uhcfr.testcoreplugin.interfaces;

/**
 * Copyright (c) 2015 - 2017 UHCFr. All rights reserved
 * This file is a part of UHCFr project.
 *
 * @author Gogume1er
 */
public interface IPacketPlayInFlying {

    void setX(double x);

    double getX();

    void setY(double y);

    double getY();

    void setZ(double z);

    double getZ();

}
