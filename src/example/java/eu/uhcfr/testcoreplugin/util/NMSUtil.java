package eu.uhcfr.testcoreplugin.util;

/**
 * Copyright (c) 2015 - 2017 UHCFr. All rights reserved
 * This file is a part of UHCFr project.
 *
 * @author Gogume1er
 */
public class NMSUtil {

    public static final String NMS_PACKAGE = "net.minecraft.server.v1_8_R3.";
}
