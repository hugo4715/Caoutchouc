package eu.uhcfr.testcoreplugin;

import eu.uhcfr.caoutchouc.CorePlugin;
import net.minecraft.launchwrapper.IClassTransformer;
import org.spongepowered.asm.launch.MixinBootstrap;
import org.spongepowered.asm.mixin.Mixins;

/**
 * Copyright (c) 2015 - 2017 UHCFr. All rights reserved
 * This file is a part of UHCFr project.
 *
 * @author Gogume1er
 */
public class TestCorePlugin implements CorePlugin, IClassTransformer {

    public TestCorePlugin() {
        MixinBootstrap.init();
        Mixins.addConfiguration("mixins.testcoreplugin.json");
    }

    @Override
    public int getTweakOrder() {
        return 0;
    }

    @Override
    public String[] getTransformerClass() {
        return new String[] {this.getClass().getName()};
    }

    @Override
    public byte[] transform(String className, String s1, byte[] bytes) {
        System.out.println(className);
        return bytes;
    }

}
