package eu.uhcfr.testcoreplugin.mixin;

import org.bukkit.plugin.InvalidPluginException;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPluginLoader;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Copyright (c) 2015 - 2017 UHCFr. All rights reserved
 * This file is a part of UHCFr project.
 *
 * @author Gogume1er
 */
@Mixin(targets = {"org.bukkit.plugin.java.PluginClassLoader"})
public abstract class MixinPluginClassLoader extends URLClassLoader {

    MixinPluginClassLoader(JavaPluginLoader loader, ClassLoader parent, PluginDescriptionFile description, File dataFolder, File file) throws InvalidPluginException, MalformedURLException {
        super(new URL[] {file.toURI().toURL()}, parent);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        System.out.println("PluginClassLoader load " + name);
        return findClass(name, true);
    }

    @Shadow
    abstract Class<?> findClass(String name, boolean checkGlobal) throws ClassNotFoundException;

}
