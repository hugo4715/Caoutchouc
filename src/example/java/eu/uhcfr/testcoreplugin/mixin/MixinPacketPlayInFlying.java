package eu.uhcfr.testcoreplugin.mixin;

import eu.uhcfr.testcoreplugin.interfaces.IPacketPlayInFlying;
import eu.uhcfr.testcoreplugin.util.NMSUtil;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

/**
 * Copyright (c) 2015 - 2017 UHCFr. All rights reserved
 * This file is a part of UHCFr project.
 *
 * @author Gogume1er
 */
@Mixin(targets = {NMSUtil.NMS_PACKAGE + "PacketPlayInFlying"})
public abstract class MixinPacketPlayInFlying implements IPacketPlayInFlying {

    @Shadow
    protected double x;
    @Shadow
    protected double y;
    @Shadow
    protected double z;

    @Override
    public double getX() {
        return x;
    }

    @Override
    public void setX(double x) {
        this.x = x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public void setY(double y) {
        this.y = y;
    }

    @Override
    public double getZ() {
        return z;
    }

    @Override
    public void setZ(double z) {
        this.z = z;
    }

}
