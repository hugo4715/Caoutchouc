package eu.uhcfr.testcoreplugin.mixin;

import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.command.defaults.PluginsCommand;
import org.spongepowered.asm.mixin.Mixin;

/**
 * Copyright (c) 2015 - 2017 UHCFr. All rights reserved
 * This file is a part of UHCFr project.
 *
 * @author Gogume1er
 */
@Mixin(PluginsCommand.class)
public abstract class MixinPluginsCommand extends BukkitCommand {

    public MixinPluginsCommand(String name) {
        super(name);
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        commandSender.sendMessage("OUUUUUUUUIIIIIII");
        return true;
    }


}
